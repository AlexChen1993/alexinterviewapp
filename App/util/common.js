

export default class common {
    //抓取device locale的keyword並轉換成我們I18n設定檔
    handleLocale(str) {
        let language = "en";
        str = str.toLowerCase();
        if(str.indexOf("hans") >= 0 || str.indexOf("cn") >= 0) {
            language = "zh";
        }
        else if(str.indexOf("zh") >= 0) {
            language = "zh";
        }
        else if(str.indexOf("en") >= 0) {
            language = "en";
        }
        
        return language;
    }

}