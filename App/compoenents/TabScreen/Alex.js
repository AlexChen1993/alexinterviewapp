import React, { Component } from 'react';
import I18n from '../../locale/I18n';
import {
  View,
  Text,
  Button,
  Image,
  StyleSheet,
  ScrollView,
} from 'react-native';

const Head = () => {
    return (
        <View style={styles.head}>
            <View style={styles.leftPartOfHead}>
                <Image style={styles.photo} source={require('../../../assets/alex.jpg')}/>
                <Text style={styles.name}>Alex</Text>
            </View>
            <View style={styles.rightPartOfHead}>
                <Text style={styles.introduction}>introduction  i'm alex. Graduate from NTUT... abcdefghijklmnopqrstuvwxyz</Text>
            </View>
        </View>
    ) 
} 
const Body = () =>{
    return (
        <View style={styles.body}>
            <Paragraph/>
        </View>
    )
}
const Paragraph = () => {
    let arr=[];
    for (let i = 0; i < 3; i++) {
        arr.push (
            <View key={i} style={styles.paragraph}>
                <Text style={styles.bodyTitleText}>title</Text>
                <Text style={styles.commonText}>paragraph</Text>
            </View>
        )
    }
    return arr;
}
const Footer = () => {
    return(
        <View style={styles.footer}>
            <Text style={styles.contact}>{I18n.t('greeting')}</Text>
        </View>
    )
}

export default class Alex extends Component{
  render(){
    return (
      <View style={styles.root} >
        <Head/>
        <Body/>
        <Footer/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
    root: {
        flex: 1
    },
    head: {
        flex: 2,
        flexDirection: 'row',
        backgroundColor: '#4bacc6',
        justifyContent: 'space-around',
        alignItems: 'center',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    leftPartOfHead: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',      
    },
    rightPartOfHead: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        marginLeft: 10
    },
    body: {
        flex: 3,
        backgroundColor: 'white',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    footer: {
        flex: 1,
        backgroundColor: '#2c6a9e',
        paddingHorizontal: 10,
        paddingVertical: 10,
    },
    photo: {
        width: 150,
        height: 150,
        borderRadius: 150,
        borderWidth: 1,
        borderColor: '#c2c2c2',
    },
    name: {
        fontSize: 20,
        color: 'white',
    },
    introduction: {
        fontSize: 14,
        color: 'white',
    },
    bodyTitleText: {
        fontSize: 18,
        fontWeight: 'bold',
        color: '#bb0101',
    },
    commonText:{
        fontSize: 14,
    },
    paragraph:{
        paddingBottom: 5,
    },
    contact: {
        color: 'white',
    }
})