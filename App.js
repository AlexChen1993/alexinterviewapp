/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Alex from './App/compoenents/TabScreen/Alex';
import Gitlab from './App/compoenents/TabScreen/Gitlab';
import Experience from './App/compoenents/TabScreen/Experience';
import Other from './App/compoenents/TabScreen/Other';
import I18n from './App/locale/I18n';
import commonUtil from './App/util/common';
const navigator = createBottomTabNavigator(
  {
    Alex: {
      screen: Alex,
      navigationOptions:({navigation})=>({
        tabBarLabel: '艾力克斯',
        tabBarIcon: <Image style = {styles.iconImage} source = {require('./assets/user.png')}/>,
      }),
    },
    Gitlab: {
      screen: Gitlab,
      navigationOptions:({navigation})=>({
        tabBarLabel: '狐狸lab',
        tabBarIcon: <Image style = {styles.iconImage} source = {require('./assets/gitlab.png')}/>
      })
    },
    Experience: {
      screen: Experience,
      navigationOptions:({navigation})=>({
        tabBarLabel:'經驗',
        tabBarIcon: <Image style = {styles.iconImage} source = {require('./assets/experience.png')}/>,
      }),
    },
    Other: {
      screen: Other,
      navigationOptions:({navigation})=>({
        tabBarLabel: '其他',
        tabBarIcon: <Image style = {styles.iconImage} source = {require('./assets/other.png')}/>
      })
    },
  },
  { 
    initialRouteName: 'Alex',
    defaultNavigationOptions: {
      tabBarVisible: true,
    },
    tabBarOptions: {
      labelStyle: {
        fontSize: 14
      },
      style: {
  
      },
      activeTintColor: 'black',
      inactiveTintColor: '#c2c2c2',
    },
  }
)
const AppContainer = createAppContainer(navigator);

export default class App extends Component{
  UNSAFE_componentWillMount() {
    this.commonUtil = new commonUtil();
    //try {
      //get device local language
      let devicelocale = I18n.currentLocale();
      //get setting language 
      // let languageSetting = await AsyncStorage.getItem('language_setting');
      // if(languageSetting !== null) {
      //   I18n.locale = languageSetting;
      // }
      // else {
      
       I18n.locale = this.commonUtil.handleLocale(devicelocale);
      // }
    //} catch (e) {
      // saving error
    //   console.log(e);
    // }
  }
  render(){
    return(
        <AppContainer/>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  iconImage: {
    height:25, 
    width:25,
  }
})